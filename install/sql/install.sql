-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 26, 2020 at 08:16 PM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bmtracking_vms`
--

-- --------------------------------------------------------

--
-- Table structure for table `acc_account`
--

CREATE TABLE `acc_account` (
  `account_id` int(11) NOT NULL,
  `sector_name` varchar(255) NOT NULL,
  `sector_type` varchar(120) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_account`
--

INSERT INTO `acc_account` (`account_id`, `sector_name`, `sector_type`, `status`, `date`) VALUES
(1, 'Sector name', 'Debit (-)', 1, '2018-04-03'),
(2, 'Sector name', 'Credit (+)', 1, '2018-04-03'),
(3, 'Sector name-1', 'Credit (+)', 1, '2018-04-03'),
(4, 'Sector name-2', 'Credit (+)', 1, '2018-04-03'),
(5, 'Sector name-3', 'Credit (+)', 1, '2018-04-03'),
(6, 'Sector name-4', 'Credit (+)', 1, '2018-04-03'),
(7, 'Sector name-1', 'Debit (-)', 1, '2018-04-03'),
(8, 'Sector name-2', 'Debit (-)', 1, '2018-04-03'),
(9, 'Sector name-3', 'Debit (-)', 1, '2018-04-03'),
(10, 'Sector name-4', 'Debit (-)', 1, '2018-04-03');

-- --------------------------------------------------------

--
-- Table structure for table `acc_bank`
--

CREATE TABLE `acc_bank` (
  `bank_id` int(11) NOT NULL,
  `bank_name` varchar(200) NOT NULL,
  `branch_name` varchar(255) NOT NULL,
  `account_number` varchar(50) NOT NULL,
  `opening_credit` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_bank`
--

INSERT INTO `acc_bank` (`bank_id`, `bank_name`, `branch_name`, `account_number`, `opening_credit`, `status`, `date`) VALUES
(1, 'AB Bank', 'Kwran bazar', '6756756756767', 20000, 1, '2018-04-03'),
(2, 'City Bank', 'Framgate Branch', '765467456745', 50000, 1, '2018-04-03'),
(3, 'Dhaka Bank', 'Mohakhali Branch', '676766', 20000, 1, '2018-04-03'),
(4, 'UCB Bank', 'Panthopath Barnch', '6767546754', 50000, 1, '2018-04-03'),
(5, 'Rupali Bank', 'Gulshan-1 Branch', '67567567', 3000, 1, '2018-04-03'),
(6, 'UCB', 'Kawran bazer', '06-6757567', 50000, 1, '2018-04-07'),
(7, 'Modumoti Bank', 'Lokkipur', '5645645645444', 10000, 1, '2018-04-07'),
(8, 'Jumuna Bank ', 'Kawran bazer', '5645645645', 70000, 1, '2018-04-07'),
(9, 'AFH Bank', 'Lokkipur', '567567567567', 10000, 1, '2018-04-07'),
(10, 'Habib Bank', 'Kawran bazer', '56456456454444', 90000, 1, '2018-04-07');

-- --------------------------------------------------------

--
-- Table structure for table `acc_company`
--

CREATE TABLE `acc_company` (
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `phone_no` varchar(30) NOT NULL,
  `fax_no` varchar(30) NOT NULL,
  `email` varchar(120) NOT NULL,
  `website` varchar(120) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_company`
--

INSERT INTO `acc_company` (`company_id`, `name`, `address`, `mobile_no`, `phone_no`, `fax_no`, `email`, `website`, `date`) VALUES
(5, 'bdtask', ' framgate   ', '01710410490', '01710410490', '576456754', 'alhassan.bdtask@gmail.com', 'bdtask.com', '2020-06-25');

-- --------------------------------------------------------

--
-- Table structure for table `acc_inflow`
--

CREATE TABLE `acc_inflow` (
  `inflow_id` int(11) NOT NULL,
  `received_date` date NOT NULL,
  `received_from` varchar(255) NOT NULL,
  `received_type` tinyint(1) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(120) DEFAULT NULL,
  `pay_order_number` varchar(120) DEFAULT NULL,
  `letter_of_credit` varchar(120) DEFAULT NULL,
  `deposit_bank_id` int(11) DEFAULT NULL,
  `account_name` varchar(255) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_inflow`
--

INSERT INTO `acc_inflow` (`inflow_id`, `received_date`, `received_from`, `received_type`, `bank_name`, `branch_name`, `account_number`, `pay_order_number`, `letter_of_credit`, `deposit_bank_id`, `account_name`, `amount`, `description`, `status`, `date`) VALUES
(1, '2018-04-04', 'Jhon Cena', 1, '', '', '', '', '', 0, 'Sector name-1', '5000', 'Description ', 1, '2018-04-03'),
(2, '2018-04-03', 'Jhon Cena', 2, 'AB Bank', 'Kwran bazar', '6756756756767', '', '', 1, 'Sector name-1', '5000', 'Description', 1, '2018-04-03'),
(3, '2018-04-07', 'Megna', 1, '', '', '', '', '', 0, 'Sector name', '2000', 'received', 1, '2018-04-07'),
(4, '2018-04-07', 'Megna ', 2, 'City Bank', 'Kawran bazer', '564564564544', '', '', 1, 'Sector name-1', '8000', 'received', 1, '2018-04-07'),
(5, '2018-04-07', 'Megna', 4, 'City Bank', 'Lokkipur', '', '', '657657', 1, 'Sector name-2', '7777', 'received', 1, '2018-04-07');

-- --------------------------------------------------------

--
-- Table structure for table `acc_invoice`
--

CREATE TABLE `acc_invoice` (
  `invoice_id` int(11) NOT NULL,
  `tracking_no` varchar(50) NOT NULL,
  `customer_name` varchar(50) NOT NULL,
  `customer_address` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `billing_address` varchar(255) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `item` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `discount` float DEFAULT NULL,
  `vat` float DEFAULT NULL,
  `grand_total` float NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `acc_outflow`
--

CREATE TABLE `acc_outflow` (
  `outflow_id` int(11) NOT NULL,
  `payment_date` varchar(50) NOT NULL,
  `payment_to` varchar(255) NOT NULL,
  `payment_type` tinyint(1) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `account_number` varchar(120) DEFAULT NULL,
  `pay_order_number` varchar(120) DEFAULT NULL,
  `letter_of_credit` varchar(120) DEFAULT NULL,
  `deposit_bank_id` int(11) DEFAULT NULL,
  `account_name` varchar(255) NOT NULL,
  `amount` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acc_outflow`
--

INSERT INTO `acc_outflow` (`outflow_id`, `payment_date`, `payment_to`, `payment_type`, `bank_name`, `branch_name`, `account_number`, `pay_order_number`, `letter_of_credit`, `deposit_bank_id`, `account_name`, `amount`, `description`, `status`, `date`) VALUES
(1, '2018-04-04', 'Cena', 2, 'AB Bank', 'Kwran bazar', '6756756756767', '', '', 1, 'Sector name', '10000', 'Description', 1, '2018-04-03'),
(2, '2018-04-07', 'megna', 2, 'City Bank', 'framgate', '564564564544', '', '', 1, 'Sector name-3', '8888', 'Received', 1, '2018-04-07'),
(3, '2018-04-07', 'partex', 3, 'City Bank', 'Kawran bazer', '', '876545678996', '', 4, 'Sector name-1', '6666', 'Received', 1, '2018-04-07'),
(4, '2018-04-07', 'Amber', 4, 'City Bank', 'framgate', '', '', '6576575', 6, 'Sector name-4', '80000', 'Received', 1, '2018-04-07'),
(5, '2018-04-07', 'Aci', 3, 'Modumoti Bank', 'Framgate', '', '876545678', '', 3, 'Sector name-2', '9999', 'Received', 1, '2018-04-07');

-- --------------------------------------------------------

--
-- Table structure for table `add_company`
--

CREATE TABLE `add_company` (
  `company_id` int(11) UNSIGNED NOT NULL,
  `company_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `company_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `company_postcode` int(10) DEFAULT NULL,
  `company_trade_reg` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `company_registration` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `company_cell` varchar(100) DEFAULT NULL,
  `company_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company_web` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company_tax` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `posting_id` int(10) NOT NULL,
  `active` int(10) NOT NULL COMMENT '0 = inactive, 1 = active, 2 = delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `add_company`
--

INSERT INTO `add_company` (`company_id`, `company_name`, `company_address`, `company_postcode`, `company_trade_reg`, `company_registration`, `company_cell`, `company_email`, `company_web`, `company_tax`, `posting_id`, `active`) VALUES
(1, 'bdtask', '237 MacDonald Street\r\nWhataupoko\r\nGisborne 4010', NULL, NULL, NULL, '(027) 3746-722', 'JaimeBThomas@teleworm.us', 'PopBooth.co.nz', NULL, 1, 1),
(2, 'Asiatic Solutions', '38 Link Road\r\nCAPE BARREN ISLAND TAS 7257', NULL, NULL, NULL, '01710410490', 'JosephJCortez@dayrep.com', 'GamingCloset.com.au', NULL, 1, 1),
(3, 'Aaronson Furniture', '784 Tree Top Lane\r\nAllentown, PA 18109', NULL, NULL, NULL, '610-288-5853', 'NicholasMRoessler@teleworm.us', 'designerfences.com', NULL, 1, 1),
(4, 'MobileCostumes', 'Vennemansweg 152\r\n7251 HX  Vorden', NULL, NULL, NULL, '06-29952405', 'TinaKProvencher@armyspy.com', 'MobileCostumes.nl', NULL, 1, 1),
(5, 'Hills Supermarkets', '33, rue Saint Germain\r\n92230 GENNEVILLIERS', NULL, NULL, NULL, '01.15.36.32.26', 'CharlesMacvitie@rhyta.com', 'RegionalStore.fr', NULL, 1, 1),
(6, 'abcd', 'abcd', NULL, NULL, NULL, '78678678', 'abcd@gmail.com', 'abcd.com', NULL, 1, 2),
(7, 'abcd', 'dfghdh', NULL, NULL, NULL, '78678678', 'abcd@gmail.com', '', NULL, 1, 1),
(8, 'Megna Group', 'Dhaka Tatola', NULL, NULL, NULL, '65-75766', 'megna@gmail.com', 'megna.com', NULL, 1, 1),
(9, 'Amberit Group', 'Gulsha Dhaka', NULL, NULL, NULL, '78-6767', 'ambergroup@gmail.com', 'www.amber.com', NULL, 1, 1),
(10, 'Zumuna task', 'Dhaka', NULL, NULL, NULL, '898-7897897', 'task@gmail.com', 'www.task.com', NULL, 1, 2),
(11, 'Zumuna task', 'Dhaka', NULL, NULL, NULL, '898-7897897', 'task@gmail.com', 'www.task.com', NULL, 1, 1),
(12, 'name', '', NULL, NULL, NULL, '', '', '', NULL, 1, 2),
(13, 'test', 'test', NULL, NULL, NULL, '695425696', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2),
(14, 'tes 237', 'Ancienne Mairie - Etoug ébé', 5278, 'hesk.PNG', '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', 'hesk.PNG', 1, 2),
(15, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(16, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(17, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(18, 'name', 'Ancienne Mairie - Etoug ébé', 5278, NULL, '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2),
(19, 'name', 'Ancienne Mairie - Etoug ébé', 5278, NULL, '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2),
(20, 'jlhlmjhml', 'klmkjm', 5278, NULL, '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='city wise station';

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`city_id`, `state_id`, `city_name`, `posting_id`, `active`) VALUES
(4, 6, 'maxco', 1, 1),
(5, 5, 'Kukuri', 1, 1),
(6, 7, 'Abbesses', 1, 1),
(7, 7, 'Alexandre Dumas', 1, 1),
(8, 6, 'Eastern Cape', 1, 1),
(9, 6, 'Free State', 1, 1),
(10, 12, 'Koltakata stand', 1, 1),
(11, 11, 'Tim Johne', 1, 1),
(12, 12, 'Koltakata stand 2', 1, 1),
(13, 14, 'Samjal Tor', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `city_city_distance`
--

CREATE TABLE `city_city_distance` (
  `distance_id` int(10) UNSIGNED NOT NULL,
  `city_id_one` int(10) NOT NULL,
  `city_id_two` int(10) NOT NULL,
  `distance` int(100) NOT NULL,
  `measurement_scale` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` int(10) NOT NULL,
  `active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_city_distance`
--

INSERT INTO `city_city_distance` (`distance_id`, `city_id_one`, `city_id_two`, `distance`, `measurement_scale`, `posting_id`, `active`) VALUES
(3, 4, 4, 30, '1', 1, 1),
(4, 5, 4, 50, '1', 1, 1),
(5, 5, 4, 50, '2', 1, 1),
(6, 9, 5, 1000, '1', 1, 1),
(7, 7, 5, 30, '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company_rent`
--

CREATE TABLE `company_rent` (
  `company_rent_id` int(10) UNSIGNED NOT NULL,
  `company_id` int(100) NOT NULL,
  `v_type_id` int(100) NOT NULL,
  `starting_station_id` int(100) NOT NULL,
  `ending_station_id` int(100) NOT NULL,
  `rent_type` int(10) NOT NULL,
  `rent` float NOT NULL,
  `vat` int(11) NOT NULL,
  `vat_status` tinyint(1) NOT NULL DEFAULT '0',
  `posting_id` int(100) NOT NULL,
  `active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_rent`
--

INSERT INTO `company_rent` (`company_rent_id`, `company_id`, `v_type_id`, `starting_station_id`, `ending_station_id`, `rent_type`, `rent`, `vat`, `vat_status`, `posting_id`, `active`) VALUES
(1, 3, 8, 4, 4, 2, 2000, 200, 0, 1, 1),
(2, 2, 14, 4, 4, 0, 30000, 300, 1, 1, 1),
(3, 5, 8, 4, 4, 2, 3000, 0, 2, 1, 1),
(4, 2, 13, 4, 4, 0, 2000, 500, 1, 1, 1),
(5, 4, 10, 4, 4, 2, 3000, 200, 1, 1, 1),
(6, 2, 20, 7, 7, 2, 9000, 4, 1, 1, 1),
(7, 3, 19, 7, 7, 0, 7000, 0, 0, 1, 1),
(8, 9, 13, 6, 7, 1, 8000, 7, 1, 1, 1),
(9, 11, 19, 6, 7, 2, 6000, 7, 1, 1, 1),
(10, 2, 19, 7, 9, 0, 4000, 5, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `driver_info`
--

CREATE TABLE `driver_info` (
  `driver_id` int(10) UNSIGNED NOT NULL,
  `driver_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_mobile` varchar(100) CHARACTER SET latin1 NOT NULL,
  `v_registration_no` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `d_license_no` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_license_picture` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_father_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `d_mother_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `d_nid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `d_nid_exp_date` date DEFAULT NULL,
  `d_nid_picture` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_join_date` date NOT NULL,
  `d_release_date` date NOT NULL,
  `d_emergency_contact_person` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_emergency_cell` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_picture` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_license_expire_date` date NOT NULL,
  `d_address_present` varchar(255) CHARACTER SET latin1 NOT NULL,
  `d_address_permanent` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 = inactive, 1 = active, 2 = delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `driver_info`
--

INSERT INTO `driver_info` (`driver_id`, `driver_name`, `d_mobile`, `v_registration_no`, `d_license_no`, `d_license_picture`, `d_father_name`, `d_mother_name`, `d_nid`, `d_nid_exp_date`, `d_nid_picture`, `d_join_date`, `d_release_date`, `d_emergency_contact_person`, `d_emergency_cell`, `d_picture`, `d_license_expire_date`, `d_address_present`, `d_address_permanent`, `posting_id`, `active`) VALUES
(3, 'Fadl Allah Jalal al Din Assaf', '(88) 528-135', '3', '56756555', '', 'Boulosgr', 'Boulos', 're56456456', NULL, '', '2018-04-01', '1970-01-01', 'smon', '(88) 65756756', 'Denve.jpg', '2018-04-02', '8457 Bakonypölöske\r\nIzabella u. 27.', '8457 Bakonypölöske\r\nIzabella u. 27.', '', 1),
(4, 'Michael L. Gowans', '425-432-8861', '3', '534-26-XXXX', '', 'Purple', 'Purp', '129659698583888', NULL, '', '2018-04-02', '1970-01-01', 'Michael', '425-432-8886', '85a894.jpg', '2018-05-18', '1715 Conifer Drive\r\nMaple Valley, WA 98038', '1715 Conifer Drive\r\nMaple Valley, WA 98038', '', 1),
(5, 'Mada Taalah Nazari', '440 9604', '5', '440 96047667', '', 'Madaian', 'Mada', '15744456456456', NULL, '', '2018-04-01', '1970-01-01', 'Aries', '657567567', '85a893.jpg', '2018-07-21', 'Heiðarbraut 18\r\n671 Kópasker', 'Heiðarbraut 18\r\n671 Kópasker', '', 1),
(6, 'Kharon Eldarkhanov', '0483 92 33 60', '5', '0483923360', '', 'Dratchev', 'Dratche', '768756456456', NULL, '', '2018-04-01', '1970-01-01', 'Dratchev', '8756756', '85a892.jpg', '2018-04-14', 'Ringlaan 75\r\n3350 Drieslinter', 'Ringlaan 75\r\n3350 Drieslinter', '', 1),
(7, 'Buvaysar Ibragimov', '76867867', '6', '6756756', '', 'Bazhaev', 'Bazhae', '56456456', NULL, '', '2018-04-01', '1970-01-01', 'Bazhaev', '657567567', 'bd.png', '2018-07-21', 'Vejlebæksvej 82\r\n4100 Ringsted', 'Vejlebæksvej 82\r\n4100 Ringsted', '', 1),
(8, 'Avtorhan Sultygo', '(11) 2360-3201', '7', '675675687', '', 'Kadiev', 'Kadie', '786876888', NULL, '', '2018-04-03', '1970-01-01', 'Kadiev', '(11)67867876', 'Denve.jpg', '2018-04-28', 'Rua Sorocaba, 268\r\nMauá-SP\r\n09370-150', 'Rua Sorocaba, 268\r\nMauá-SP\r\n09370-150', '', 1),
(9, 'Zorislav Babi?', '044 133 9601', '10', '67567566', '', 'Lon?ar', 'Lon?', '786876855', NULL, '', '2018-04-02', '1970-01-01', 'Lon?ar', '044 133 9601', '85a891.jpg', '2018-04-19', 'Kunnankuja 66\r\n92930 PYHÄNTÄ', 'Kunnankuja 66\r\n92930 PYHÄNTÄ', '', 1),
(10, 'Miodrag Nikoli?', '458 6346', '3', '7678675', '', ' Dalbrautrrt', ' Dalbraut', '786876866', NULL, '', '2018-04-01', '1970-01-01', 'Dalbraut', '458 6347', '85a89.jpg', '2018-04-21', 'Bjarg Dalbraut 84\r\n370 Búðardalur', 'Bjarg Dalbraut 84\r\n370 Búðardalur', '', 1),
(11, 'Sven Tkal?i?', '676576576', '7', '765756756', '', 'Bistricahrn', 'Bistrica', '6578567567', NULL, '', '2018-04-01', '1970-01-01', 'Sven', '5675765', 'men-200x200.jpg', '2018-04-21', 'Tavcarjeva 91\r\n2310 Slovenska Bistrica', 'Tavcarjeva 91\r\n2310 Slovenska Bistrica', '', 1),
(12, 'Naji Saleem Shalhoub', '06-51600486', '3', '6756756', '', 'Naji ', 'Abadi', '7868768768', NULL, '', '2018-04-01', '1970-01-01', 'Malden', '06-51600486', 'p3-bhutan-a-20131226-200x200.jpg', '2018-04-27', 'Hatertseweg 57\r\n6581 KE  Malden', 'Hatertseweg 57\r\n6581 KE  Malden', '', 1),
(13, 'Sulaiman Fahad Kassis', '89 63 18', '4', '5645644467', '', 'Nassar merd', 'Lawn Care', '6567654565456', NULL, '', '2018-04-07', '1970-01-01', 'jhon abrnah', '8765468', 'images1.jpg', '2018-04-19', 'North Amirica', 'North Amirica', '', 1),
(14, 'Brio', '690145010', '4', '4562612656', '', 'jj', 'jv', '156324568', NULL, '', '2020-06-02', '2020-06-30', 'jj', '690145011', 'op905_mp4_snapshot_11_20_801.jpg', '2020-06-30', 'Ancienne Mairie - Etoug ébé', 'Ancienne Mairie - Etoug ébé', '', 2),
(15, 'jt', '690145010', '2', '56446546', '', 'jj', 'jv', '165656264', NULL, 'hesk.PNG', '2020-06-01', '2020-06-17', 'jj', '454651621', 'vlcsnap-2018-12-01-05h59m17s677.png', '2020-06-30', 'Ancienne Mairie - Etoug ébé', 'Ancienne Mairie - Etoug ébé', '', 0),
(16, 'test', '690145010', '3', '46568', 'hesk1.PNG', 'tut', 'tum', '456123', '2020-06-17', 'Barcelona_wbg1.png', '2020-06-10', '2020-06-19', 'jj', '695213548', 'Barcelona_wbg.png', '2020-06-10', 'Ndogbong - Commissariat 10e', 'Ancienne Mairie - Etoug ébé', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `expense_data`
--

CREATE TABLE `expense_data` (
  `transection_id` int(11) NOT NULL,
  `import_export` tinyint(2) DEFAULT NULL,
  `trip_link_id` varchar(255) DEFAULT NULL,
  `expense_group` int(11) NOT NULL,
  `expense_id` int(11) DEFAULT NULL,
  `expense_serial` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `v_id` int(11) DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `posting_id` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_data`
--

INSERT INTO `expense_data` (`transection_id`, `import_export`, `trip_link_id`, `expense_group`, `expense_id`, `expense_serial`, `date`, `v_id`, `quantity`, `amount`, `posting_id`, `active`) VALUES
(2449, 0, 'M6TIB0NGUL', 1, 7, '', '2020-06-26', 2, 0, 0, '1', 1),
(2448, 0, 'M6TIB0NGUL', 1, 6, '', '2020-06-26', 2, 2, 15000, '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `expense_list`
--

CREATE TABLE `expense_list` (
  `expense_id` int(10) UNSIGNED NOT NULL,
  `expense_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `expense_group` int(11) NOT NULL,
  `posting_id` int(11) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `expense_list`
--

INSERT INTO `expense_list` (`expense_id`, `expense_name`, `expense_group`, `posting_id`, `active`) VALUES
(1, 'Landscape architect', 2, 1, 1),
(2, 'Tips', 3, 1, 1),
(3, 'Comission', 3, 1, 1),
(4, 'Tips', 4, 1, 1),
(5, 'Comission', 5, 1, 1),
(6, 'Police Chanda', 1, 1, 1),
(7, 'Accident', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` int(11) UNSIGNED NOT NULL,
  `phrase` text NOT NULL,
  `english` text NOT NULL,
  `francais` text,
  `spanish` text,
  `faviicon` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `phrase`, `english`, `francais`, `spanish`, `faviicon`) VALUES
(374, 'addvehicletype', 'Add Vehicle Type', 'Ajouter un type de véhicule', NULL, NULL),
(375, 'addvehicle', 'Add Vehicle ', 'Ajouter un véhicule', NULL, NULL),
(376, 'addvehiclefuelrate', 'Add Vehicle Fuel Rate', 'Ajouter le taux de carburant du véhicule', NULL, NULL),
(378, 'vehicletypelist', 'Vehicles Type List', 'Liste des types de véhicules', NULL, NULL),
(379, 'vehiclelist', 'Vehicles List', 'Liste des véhicules', NULL, NULL),
(380, 'vehicle', 'Vehicles', 'Véhicules', NULL, NULL),
(381, 'driver', 'Drivers', 'Chauffeurs', NULL, NULL),
(382, 'adddriver', 'Add Driver', 'Ajouter un chauffeur', '', NULL),
(383, 'driverlist', 'Driver List', 'Liste des chauffeurs', NULL, NULL),
(384, 'trip', 'Trips', 'Voyages', NULL, NULL),
(385, 'triplist', 'Trips List', 'Liste des voyages', NULL, NULL),
(386, 'localtriplist', 'Local Trip List', 'Liste des voyages locaux', NULL, NULL),
(387, 'tripentry', 'Trip Entry', 'Ajouter un voyage', NULL, NULL),
(388, 'localtripentry', 'Local Trip Entry', 'Ajouter un voyage local', NULL, NULL),
(389, 'customers', 'Customers', 'Clients', NULL, NULL),
(390, 'addcompany', 'Add Company', 'Ajouter une entreprise', '', NULL),
(391, 'companylist', 'Company List', 'Liste des entreprises', NULL, NULL),
(392, 'addprovider', 'Add provider', 'Ajouter un fournisseur', '', NULL),
(393, 'providerlist', 'Provider List', 'Liste des fournisseurs', NULL, NULL),
(394, 'fitness', 'Fitness', 'Aptitudes', NULL, NULL),
(395, 'addvehiclefitness', 'Add Vehicle Fitness', 'Ajouter l\'aptitude d\'un véhicule', NULL, NULL),
(396, 'fitnesslist', 'Fitness List', 'Liste des aptitudes', NULL, NULL),
(397, 'fuel', 'Fuel management', 'Gestion du Carburant', NULL, NULL),
(398, 'fuelratelist', 'Fuel Rate List', 'Liste des taux de carburant', NULL, NULL),
(399, 'expense', 'Expenses', 'Dépenses', NULL, NULL),
(400, 'expenseentry', 'Expense Entry', 'Entrée de la dépense', NULL, NULL),
(401, 'expenselist', 'Expenses List', 'Liste des dépenses', NULL, NULL),
(402, 'expensetypelist', 'Expense Type List', 'Liste des types de dépenses', NULL, NULL),
(403, 'addexpensetype', 'Add Expense Type', 'Ajouter un type de dépense', '', NULL),
(404, 'stationsetup', 'Station Setup', 'Configuration de la station', NULL, NULL),
(405, 'addstateinformation', 'Add State Information', 'Ajouter des informations d\'état', '', NULL),
(406, 'statelist', 'State List', 'Liste des états', NULL, NULL),
(407, 'addstationinformation', 'Add Station Information', 'Ajouter des informations sur la station', '', NULL),
(408, 'stationlist', 'Station List', 'Liste des stations', NULL, NULL),
(409, 'addstationdistense', 'Add Station Distense', 'Ajouter une station de distribution', '', NULL),
(410, 'stationdistancelist', 'Station Distance list', 'Liste des distances entre les stations', NULL, NULL),
(411, 'accounting', 'Accounting', 'Comptabilité', 'accuting', NULL),
(412, 'bankinformation', 'Bank Information', 'Information Bancaire', NULL, NULL),
(413, 'accountinformation', 'Account Information', 'Informations sur le compte', 'demo', NULL),
(414, 'companyinformation', 'Company Information', 'Information sur l\'entreprise', NULL, NULL),
(415, 'invoiceinformation', 'Invoice Information', 'Informations sur la facture', NULL, NULL),
(416, 'inflow', 'Rececipt Information', 'Informations sur le recu', NULL, NULL),
(417, 'outflow', 'Payment Information', 'Informations du paiement', NULL, NULL),
(418, 'accountreports', 'Account Reports', 'Rapports sur le compte', '', NULL),
(419, 'reports', 'Reports', 'Rapports', NULL, NULL),
(420, 'generalereports', 'Generale Reports', 'Rapports généraux', NULL, NULL),
(421, 'expensereports', 'Expense Reports', 'Rapports des dépenses', NULL, NULL),
(422, 'balancesheet', 'Balance Sheet', 'Bilan', NULL, NULL),
(423, 'companybill', 'Company Bill', 'Facture de la société', NULL, NULL),
(424, 'settings', 'Settings', 'Paramètres', NULL, NULL),
(425, 'languagesetting', 'Language Setting', 'Paramètres de langue', NULL, NULL),
(426, 'softwaresetting', 'Software Setting', 'Configuration du logiciel', NULL, NULL),
(427, 'alertcenter', 'Alert Center', 'Centre d\'alerte', NULL, NULL),
(428, 'seefullalertdetails', 'See Full Alert Details', 'Voir les détails complets de l\'alerte', NULL, NULL),
(429, 'admin', 'Admin', 'Admin', NULL, NULL),
(430, 'operator', 'Operator', 'Opérateur', NULL, NULL),
(431, 'appsetting', 'App Setting', 'Paramétrage de l\'application', NULL, NULL),
(432, 'adduser', 'Add User', 'Ajouter un utilisateur', '', NULL),
(433, 'viewuser', 'View User', 'Voir  un utilisateur', NULL, NULL),
(434, 'editprofile', 'Edit Profile', 'Modifier le profil', NULL, NULL),
(435, 'logout', 'Logout', 'Se déconnecter', NULL, NULL),
(436, 'weeklytripandfarerent', 'Weekly Trip And Fare-rent', 'Voyage hebdomadaire et tarif de location', NULL, NULL),
(437, 'businesschart', 'Business Chart', 'Graphique des activités', NULL, NULL),
(438, 'vehicletypecreate', 'Vehicle Type Create', 'Créer un type de véhicule', NULL, NULL),
(439, 'yes', 'Yes', 'Oui', NULL, NULL),
(440, 'no', 'No', 'Non', NULL, NULL),
(441, 'cancel', 'Cancel', 'Annuler', NULL, NULL),
(442, 'save', 'Save', 'Sauvegarder', NULL, NULL),
(443, 'vehicleinformationcreate', 'Vehicle Information Create', 'Créer des informations sur le véhicule', NULL, NULL),
(444, 'vehicleinformationupdate', 'Vehicle Information Update', 'Mise à jour des informations sur le véhicule', NULL, NULL),
(445, 'registrationnumber', 'Registration Number', 'Numéro d\'enregistrement', NULL, NULL),
(446, 'chassisno', 'Chassis No', 'N ° de châssis', NULL, NULL),
(447, 'modelno', 'Model No', 'Numéro de modèle', NULL, NULL),
(448, 'engineno', 'Engine No', 'N ° du Moteur', NULL, NULL),
(449, 'vehicletype', 'Vehicle Type', 'Types de véhicule', NULL, NULL),
(450, 'ownershiphire', 'Own / Hire', 'Location / propriété', NULL, NULL),
(451, 'isactive', 'Is Active', 'Est actif', NULL, NULL),
(452, 'fuelrateupdate', 'Fuel Rate Update', 'Mise à jour des taux de carburant', NULL, NULL),
(453, 'fuelratecreate', 'Fuel Rate Create', 'Création de taux de carburant', NULL, NULL),
(454, 'fuelperkilo', 'Fuel Per Liter', 'Carburant par litre', NULL, NULL),
(455, 'fuelrate', 'Fuel Rate', 'Taux de carburant', NULL, NULL),
(456, 'slno', 'SL No', 'Numéro SL', NULL, NULL),
(457, 'status', 'Status', 'Statut', NULL, NULL),
(458, 'vehicleinformation', 'Vehicle Information', 'Informations sur le véhicule', NULL, NULL),
(459, 'owner', 'Owner', 'Propriétaire', NULL, NULL),
(460, 'action', 'Action', 'Action', '', NULL),
(461, 'print', 'Print', 'Imprimer', NULL, NULL),
(462, 'active', 'Active', 'Actif', '', NULL),
(463, 'inactive', 'Inactive', 'Inactif', NULL, NULL),
(464, 'edit', 'Edit', 'Editer', NULL, NULL),
(465, 'delete', 'Delete', 'Supprimer', NULL, NULL),
(466, 'driverinformationupdate', 'Driver Information Update', 'Mise à jour des informations sur le chauffeur', NULL, NULL),
(467, 'adddriverinformation', 'Add Driver Information', 'Ajouter des informations sur le chauffeur', '', NULL),
(468, 'drivername', 'Driver Name', 'Nom du chauffeur', NULL, NULL),
(469, 'mobilenumber', 'Mobile Number', 'Numéro mobile', NULL, NULL),
(470, 'vehicleregistrationno', 'Vehicle Registration No', 'Numéro d\'immatriculation du véhicule', NULL, NULL),
(471, 'licensenumber', 'License Number', 'Numéro de permis de conduire', NULL, NULL),
(472, 'licenseexpiredate', 'License Expire Date', 'Date d\'expiration du permis de conduire', NULL, NULL),
(473, 'fathername', 'FatherName', 'Nom du père', NULL, NULL),
(474, 'mothername', 'Mother Name', 'Nom de la mère', NULL, NULL),
(475, 'nid', 'NID', 'CNI', NULL, NULL),
(476, 'presentaddress', 'Present Address', 'Adresse actuelle', NULL, NULL),
(477, 'permanentaddress', 'Permanent Address', 'Addresse permanente', NULL, NULL),
(478, 'joiningdate', 'Joining Date', 'Date d\'inscription', NULL, NULL),
(479, 'releasedate', 'Release Date', 'Date de sortie', NULL, NULL),
(480, 'referenceperson', 'Reference Person', 'Personne de référence', NULL, NULL),
(481, 'cellnumber', 'Cell Number', 'Numéro de portable', NULL, NULL),
(482, 'driverpicture', 'Driver Picture', 'Photo du chauffeur', NULL, NULL),
(483, 'view', 'View', 'Voir', NULL, NULL),
(484, 'driverinformation', 'Driver Information', 'Informations sur le chauffeur', NULL, NULL),
(485, 'dailytripinformantion', 'Daily Trip Informantion', 'Informations sur le voyage quotidien', NULL, NULL),
(486, 'alltripinformantion', 'All Trip Informantion', 'Toutes les informations sur le voyage	', NULL, NULL),
(487, 'date', 'Date', 'Date', NULL, NULL),
(488, 'triplinkid', 'Trip Link Id', 'Id du lien de voyage', NULL, NULL),
(489, 'companyname', 'Company Name', 'Nom de l\'entreprise', NULL, NULL),
(490, 'startpoint', 'Start Point', 'Point de départ', NULL, NULL),
(491, 'endpoint', 'End Point', 'Point d\'arrivée', NULL, NULL),
(492, 'contactrent', 'Contact Rent', 'Contact de location', NULL, NULL),
(493, 'farerent', 'Fare Rent', 'Location de l\'immeuble', NULL, NULL),
(494, 'profit', 'Profit', 'Profit', NULL, NULL),
(495, 'advance', 'Advance', 'Avance', NULL, NULL),
(496, 'balance', 'Balance', 'Solde', NULL, NULL),
(497, 'grandtotal', 'Grand Total', 'Grand total', NULL, NULL),
(498, 'importtriplist', 'Import Trip List', 'Importer la liste des voyages', NULL, NULL),
(499, 'printall', 'Print All', 'Imprimer tout', NULL, NULL),
(500, 'tripexpense', 'Trip Expense', 'Dépense de voyage', NULL, NULL),
(501, 'expensename', 'Expense Name', 'Nom de la dépense', NULL, NULL),
(502, 'quantity', 'Quantity', 'Quantité', NULL, NULL),
(503, 'amount', 'Amount', 'Montant', NULL, NULL),
(504, 'total', 'Total', 'Total', NULL, NULL),
(505, 'close', 'Close', 'Fermer', NULL, NULL),
(506, 'exporttriplist', 'Export Trip List', 'Exporter la liste du voyage', NULL, NULL),
(507, 'localtripinformation', 'Local Trip Information', 'Informations de voyage local', NULL, NULL),
(508, 'updatetripentry', 'Update Trip Entry', 'Mise à jour d\'un voyage', NULL, NULL),
(509, 'createtripintry', 'Add Trip Entry', 'Ajouter un voyage', NULL, NULL),
(510, 'particular', 'Particular', 'Particulier', NULL, NULL),
(511, 'expensegroup', 'Expense Group', 'Groupe de dépenses', NULL, NULL),
(512, 'farerate', 'Fare Rate', 'Tarif', NULL, NULL),
(513, 'ratecontactrate', 'Rate / Contact Rate', 'Taux de contact', NULL, NULL),
(514, 'exportimport', 'Export Import', 'Exporter Importer', NULL, NULL),
(515, 'selectvehicleregistrationno', 'Select Vehicle Registration No', 'Sélectionnez le numéro d\'immatriculation du véhicule', NULL, NULL),
(516, 'hirevehicleregno', 'Hire Vehicle Reg. No', 'Numéro d\'immatriculation du véhicule de location', NULL, NULL),
(517, 'export', 'Export', 'Exporter', NULL, NULL),
(518, 'selectanoption', 'Select An Option', 'Selectionner une option', NULL, NULL),
(519, 'import', 'Import', 'Importer', NULL, NULL),
(520, 'importexport', 'Import & Export', 'Importer et Exporter', NULL, NULL),
(521, 'contactcompany', 'Contact Company', 'Contact de la compagnie', NULL, NULL),
(522, 'otherscompany', 'Others Company', 'Autres compagnies', NULL, NULL),
(523, 'selectstation', 'Select Station', 'Choisir une station', NULL, NULL),
(524, 'triptype', 'Trip Type', 'Type de voyage', NULL, NULL),
(525, 'ownsingle', 'Own Single', 'Avoir en unique', NULL, NULL),
(526, 'owndouble', 'Own Double', 'Avoir en double', NULL, NULL),
(527, 'hiresingle', 'Hire single', 'Embaucher en unique', NULL, NULL),
(528, 'hiredouble', 'Hire Double', 'Embaucher en double', NULL, NULL),
(529, 'rentsingle', 'Rent Single', 'Louer en unique', NULL, NULL),
(530, 'rentdouble', 'Rent Double', 'Louer en double', NULL, NULL),
(531, 'selectdistrict', 'Select District', 'Choisir un district', NULL, NULL),
(532, 'createlocaltripentry', 'Create Local Trip Entry', 'Créer un voyage local', NULL, NULL),
(533, 'localtrip', 'Local Trip', 'Voyage local', NULL, NULL),
(534, 'local', 'Local', 'Local', NULL, NULL),
(535, 'othercompany', 'Other Company', 'Autre companie/entreprise', NULL, NULL),
(536, 'tripexpens', 'Trip Expens', 'Dépense du voyage', NULL, NULL),
(537, 'companyupdate', 'Update Company', 'Mise à jour de l\'entreprise', NULL, NULL),
(538, 'companycreate', 'Add Company', 'Ajouter une entreprise', NULL, NULL),
(539, 'name', 'Name', 'Nom', NULL, NULL),
(540, 'address', 'Address', 'Adresse géographique', '', NULL),
(541, 'email', 'Email', 'Email', NULL, NULL),
(542, 'companyweb', 'Company Web', 'Compagnie Web', NULL, NULL),
(543, 'nodatafound', 'No Data Found', 'Aucune donnée trouvée', NULL, NULL),
(544, 'updatecompanyrent', 'Update Company Rent', 'Mettre à jour le loyer de l\'entreprise', NULL, NULL),
(545, 'renttype', 'Rent Type', 'Type de location', NULL, NULL),
(546, 'rent', 'Rent', 'Loyer', NULL, NULL),
(547, 'vatstatus', 'Vat Status', 'Statut de la TVA', NULL, NULL),
(548, 'vat', 'Vat', 'TVA', NULL, NULL),
(549, 'totalrent', 'Total Rent', 'Loyer total', NULL, NULL),
(550, 'selectvehiclttype', 'Select Vehicle Type', 'Choisir un type de véhicule', NULL, NULL),
(551, 'selectcompanyname', 'Select Company Name', 'Choisir un nom de compagnie', NULL, NULL),
(552, 'selectstartingpoint', 'Select Starting Point', 'Choisir un point de début', NULL, NULL),
(553, 'selectendingpoint', 'Select Ending Point', 'Choisir un point de fin', NULL, NULL),
(554, 'vehicalfitness', 'Vehical Fitness', 'Aptitude véhémique', NULL, NULL),
(555, 'issuedate', 'Issue Date', 'Date de délivrance', NULL, NULL),
(556, 'expiredate', 'Expire Date', 'Date d\'expiration', NULL, NULL),
(557, 'registration', 'Registration', 'Inscription', NULL, NULL),
(558, 'insurance', 'Insurance', 'Assurance', NULL, NULL),
(559, 'taxtoken', 'Tax Token', 'Jeton fiscal', NULL, NULL),
(560, 'rootpermit', 'Root Permit', 'Permis de racine', NULL, NULL),
(561, 'regissue', 'Reg Issue', 'Délivrance de l\'enregistrement', NULL, NULL),
(562, 'reg_expire', 'Reg Expire', 'Expiration de l\'enregistrement', NULL, NULL),
(563, 'fitnessissue', 'Fitness Issue', 'Délivrance de l\'aptitude', NULL, NULL),
(564, 'fitnessexpire', 'Fitness Expire', 'Expiration de l\'aptitude', NULL, NULL),
(565, 'insuranceissue', 'Insurance Issue', 'Délivrance de l\'assurance', NULL, NULL),
(566, 'insuranceexpire', 'Insurance Expire', 'Expiration de l\'assurance', NULL, NULL),
(567, 'taxissue', 'Tax Issue', 'Délivrance de la taxe', NULL, NULL),
(568, 'tax_expire', 'Tax Expire', 'Expiration de la taxe', NULL, NULL),
(569, 'permitissue', 'Permit Issue', 'Délivrance du permis', NULL, NULL),
(570, 'permitexpire', 'Permit Expire', 'Expiration du permis', NULL, NULL),
(571, 'fuelissue', 'Fuel Issue', 'Délivrance du carburant', NULL, NULL),
(572, 'fuelexpire', 'Fuel Expire', 'Expiration du carburant', NULL, NULL),
(573, 'fitnessinformation', 'Fitness information', 'Informations d\'aptitude', NULL, NULL),
(574, 'vehiclefuelrate', 'Vehicle Fuel Rate', 'Taux de carburant des véhicules', NULL, NULL),
(575, 'registrationno', 'Registration No', 'N° d\'inscription', NULL, NULL),
(576, 'expensescreate', 'Expenses Create', 'Ajout de dépenses', NULL, NULL),
(577, 'expenseserial', 'Expense Serial', 'Série de dépenses', NULL, NULL),
(578, 'expensetype', 'Expense Type', 'Type de dépense', NULL, NULL),
(579, 'expensesupdate', 'Expenses Update', 'Mise à jour des dépenses', NULL, NULL),
(580, 'expensecreate', 'Expense Create', 'Ajout d\'une dépense', NULL, NULL),
(581, 'stateinformationupdate', 'State Information Update', 'Mise à jour des informations sur l\'état', NULL, NULL),
(582, 'addstate', 'Add state', 'Ajout d\'un état', '', NULL),
(583, 'state', 'State', 'Etat', NULL, NULL),
(584, 'districtinformation', 'District Information', 'Informations du district', NULL, NULL),
(585, 'districtname', 'District Name', 'Nom du district', NULL, NULL),
(586, 'stationinformationupdate', 'Station Information Update', 'Mise à jour des informations d\'une station', NULL, NULL),
(587, 'station', 'Station', 'Station', NULL, NULL),
(588, 'stationname', 'Station Name', 'Nom de la station', NULL, NULL),
(589, 'stationinformation', 'Station Information', 'Informations de la station', NULL, NULL),
(590, 'citytocitydistenceupdate', 'CityTo City Distence Update', 'Mise à jour de la distance de ville à ville', NULL, NULL),
(591, 'citytocitydistencecreate', 'City To City Distence Create', 'Ajout d\'une distance de ville à ville', NULL, NULL),
(592, 'measurementscale', 'Measurement Scale', 'Echelle de mesure', NULL, NULL),
(593, 'selectvehiclemodel', 'Select Vehicle Model', 'Choisir un modèle de véhicule', NULL, NULL),
(594, 'distence', 'Distence', 'Distance', NULL, NULL),
(595, 'stationtostationdistanceinformation', 'Station Distance List', 'Liste des distances de stations', NULL, NULL),
(596, 'bankname', 'Bank Name', 'Nom de la banque', NULL, NULL),
(597, 'branchname', 'Branch Name', 'Nom de la branche', NULL, NULL),
(598, 'accountnumber', 'Account Number', 'Numéro de compte', '', NULL),
(599, 'openingcredit', 'Opening Credit', 'Crédit d\'ouverture', NULL, NULL),
(600, 'addbank', 'Add Bank', 'Ajouter une banque', '', NULL),
(601, 'sectorname', 'Sector Name', 'Nom du secteur', NULL, NULL),
(602, 'sectortype', 'Sector Type', 'Type de secteur', NULL, NULL),
(603, 'addaccount', 'Add Account', 'Ajouter un compte', '', NULL),
(604, 'creditaccount', 'Credit Account', 'Créditer le compte', NULL, NULL),
(605, 'debitaccount', 'Debit Account', 'Débiter le compte', NULL, NULL),
(606, 'companyaddress', 'Company Address', 'Adresse de la compagnie', NULL, NULL),
(607, 'mobilenumbner', 'Mobile Numbner', 'Numéro mobile', NULL, NULL),
(608, 'phonenumber', 'Phone Number', 'Numéro de téléphone', NULL, NULL),
(609, 'faxnumber', 'Fax Number', 'Numéro de fax', NULL, NULL),
(610, 'inflowinformation', 'Rececipt Information', 'Informations du reçu', NULL, NULL),
(611, 'addinflow', 'Add Rececipt', 'Ajouter un reçu', '', NULL),
(612, 'receiveddate', 'Received Date', 'Date du reçu', NULL, NULL),
(613, 'receivedfrom', 'Received From', 'Reçu de', NULL, NULL),
(614, 'receivedtype', 'Received Type', 'Type de reçu', NULL, NULL),
(615, 'cash', 'Cash', 'Espèces', NULL, NULL),
(616, 'cheque', 'Cheque', 'Chèque', NULL, NULL),
(617, 'payorder', 'Payorder', 'Ordre de paiement', NULL, NULL),
(618, 'lc', 'LC', 'LC', NULL, NULL),
(619, 'description', 'Description', 'Description', NULL, NULL),
(620, 'accountname', 'Account Name', 'Nom du compte', '', NULL),
(621, 'depositbankname', 'Deposit Bank Name', 'Nom de la banque de dépôt', NULL, NULL),
(622, 'payordernumber', 'Payorder Number', 'Numéro de l\'ordre de paiement', NULL, NULL),
(623, 'outflowinformation', 'Payment Information', 'Informations de paiement', NULL, NULL),
(624, 'addoutflow', 'Add Payment', 'Ajouter un paiement', '', NULL),
(625, 'paymentdate', 'Payment Date', 'Date de paiement', NULL, NULL),
(626, 'paymentto', 'Payment To', 'Paiement à', NULL, NULL),
(627, 'paymenttype', 'Payment Type', 'Type de paiement', NULL, NULL),
(628, 'lastonemonth', 'Last One Month', 'Mois dernier', NULL, NULL),
(629, 'lastthreemonth', 'Last Three Month', 'Trois derniers mois', NULL, NULL),
(630, 'lastsixmonth', 'Last Six Month', 'Six derniers mois', NULL, NULL),
(631, 'lastoneyear', 'Last One Year', 'Année dernière', NULL, NULL),
(632, 'datetodate', 'Date To Date', 'Date à date', NULL, NULL),
(633, 'generate', 'Generate', 'Générer', NULL, NULL),
(634, 'report', 'Report', 'Rapport', NULL, NULL),
(635, 'customername', 'Customer Name', 'Nom du client', NULL, NULL),
(636, 'dailyallvehicleperformance', 'Daily All Vehicle performance', 'Performance journalière de tous les véhicules', NULL, NULL),
(637, 'dailysinglevehicleperformance', 'Daily Single Vehicle performance', 'Performance journalière d\'un véhicule', NULL, NULL),
(638, 'allvehicleperformance', 'All Vehicle performance', 'Performance de tous les véhicules', NULL, NULL),
(639, 'vehiclewiseperformance', 'Vehicle wise performance', 'Performance du véhicule', NULL, NULL),
(640, 'driverwiseperformance', 'Driver wise performance', 'Performance du chauffeur', NULL, NULL),
(641, 'companywiseperformance', 'Company wise performance', 'Performance de la compagnie', NULL, NULL),
(642, 'datetodatecompanywiseperformance', 'Date to date Company wise performance', 'Performance de la compagnie date à date', NULL, NULL),
(643, 'datetodatedriverwiseperformance', 'Date to date Driver wise performance', 'Performance du chauffeur date à date', NULL, NULL),
(644, 'datetodatesinglevehicleperformance', 'Date to date single vehicle performance', 'Performance d\'un véhicule date à date', NULL, NULL),
(645, 'datetodateallvehicleperformance', 'Date to date all Vehicle performance', 'Performance de tous les véhicules date à date', NULL, NULL),
(646, 'contact_rent', 'Rate / Contact Rate', 'Taux', NULL, NULL),
(647, 'due', 'Due', 'Redevance', NULL, NULL),
(648, 'dailyexpenseallvehicle', 'Daily Expense All Vehicle', 'Dépense journalière de tous les véhicules', NULL, NULL),
(649, 'dailyexpensesinglevehicle', 'Daily Expense Single Vehicle', 'Dépense journalière d\'un véhicule', NULL, NULL),
(650, 'allvehicleexpense', 'All Vehicle Expense', 'Dépense de tous les véhicules', NULL, NULL),
(651, 'vehiclewiseexpense', 'Vehicle wise Expense', 'Dépense du véhicule', NULL, NULL),
(652, 'datetodatesinglevehicleexpense', 'Date to date single vehicle Expense', 'Dépense d\'un véhicule date à date', NULL, NULL),
(653, 'datetodateallvehicleexpense', 'Date to date all Vehicle Expense', 'Dépense de tous les véhicules date à date', NULL, NULL),
(654, 'particularwiseexpense', 'Particular Wise Expense', 'Dépense partiulière', NULL, NULL),
(655, 'datetodateparticularwiseexpense', 'Date to date Particular Wise Expense', 'Dépense particulière à ce jour', NULL, NULL),
(656, 'regularexpense', 'Regular Expense', 'Dépense régulière', NULL, NULL),
(657, 'datetodateregularexpense', 'Date to date Regular Expense', 'Dépense quotidienne', NULL, NULL),
(658, 'maintenanceexpense', 'Maintenance Expense', 'Frais de  maintenance', NULL, NULL),
(659, 'datetodatemaintenanceexpense', 'Date to date Maintenance Expense', 'Dépenses de maintenance à ce jour', NULL, NULL),
(660, 'officeexpense', 'Office Expense', 'Dépense de bureau', NULL, NULL),
(661, 'datetodateofficeexpense', 'Date to date Office Expense', 'Dépenses de bureau à ce jour', NULL, NULL),
(662, 'garageexpense', 'Garage Expense', 'Frais de garage', NULL, NULL),
(663, 'datetodategarageexpense', 'Date to date Garage Expense', 'Dépenses de garage à ce jour', NULL, NULL),
(664, 'othersexpense', 'Others Expense', 'Autres dépenses', NULL, NULL),
(665, 'datetodateothersexpense', 'Date to date others Expense', 'Dépense quotidienne des autres', NULL, NULL),
(666, 'ownervehicleexpense', 'Owner vehicle Expense', 'Frais de véhicule du propriétaire', NULL, NULL),
(667, 'hirevehicleexpense', 'Hire vehicle Expense', 'Frais de location de véhicule', NULL, NULL),
(668, 'vehiclewiseparticularexpense', 'Vehicle wise Particular Expense', 'Dépense particulière pour le véhicule', NULL, NULL),
(669, 'datetodatevehiclewiseparticularexpense', 'Date to date vehicle wise Particular Expense', 'Date à ce jour pour le véhicule avec Dépense particulière', NULL, NULL),
(670, 'regular', 'Regular', 'Regulier', NULL, NULL),
(671, 'maintenance', 'Maintenance', 'Maintenance', NULL, NULL),
(672, 'others', 'Others', 'Autres', NULL, NULL),
(673, 'office', 'Office', 'Bureau', NULL, NULL),
(674, 'garage', 'Garage', 'Garage', NULL, NULL),
(675, 'profit1', 'Profit 1', 'Profit 1', NULL, NULL),
(676, 'profit2', 'Profit 2', 'Profit 2', NULL, NULL),
(677, 'totalprofit', 'Total Profit', 'Total Profit', NULL, NULL),
(678, 'netbalanceprofit', 'Net Balance / Profit', 'Solde net / Bénéfice', NULL, NULL),
(679, 'contactrate', 'Contact Rate', 'Taux de contact', NULL, NULL),
(680, 'totalexpense', 'Total Expense', 'Dépense totale', NULL, NULL),
(681, 'to', 'To', 'De', NULL, NULL),
(682, 'exportimportlocal', 'Export - Import - Local', 'Exporter - Importer - Local', NULL, NULL),
(683, 'mobileno', 'Mobile No', 'N° Mobile', NULL, NULL),
(684, 'emailaddress', 'Email Address', 'Adresse mail', NULL, NULL),
(685, 'website', 'Website', 'Site web', NULL, NULL),
(686, 'exprtimport', 'Exprt & Import', 'Exporter et importer', NULL, NULL),
(687, 'pdf', 'PDF', 'PDF', NULL, NULL),
(688, 'addlanguagename', 'Add Language Name', 'Ajouter un nom de langue', '', NULL),
(689, 'language', 'Language', 'Langue', NULL, NULL),
(690, 'activestatus', 'Active Status', 'Statut actif', '', NULL),
(691, 'addphrase', 'Add Phrase', 'Ajouter une phrase', '', NULL),
(692, 'languagelist', 'Language List', 'liste des langues', NULL, NULL),
(693, 'phrasename', 'Phrase Name', 'Nom de la phrase', NULL, NULL),
(694, 'phrase', 'Phrase', 'Phrase', NULL, NULL),
(695, 'label', 'Label', 'Etiquette', NULL, NULL),
(696, 'reset', 'Reset', 'Réinitialiser', NULL, NULL),
(697, 'setting', 'Setting', 'Paramètre', NULL, NULL),
(698, 'ltr', 'LTR', 'LTR', NULL, NULL),
(699, 'rtr', 'RTR', 'RTR', NULL, NULL),
(700, 'selectstate', 'Select State', 'Sélectionnez l\'État', NULL, NULL),
(701, 'vehicleregistrationnumber', 'Vehicle Registration Number', 'Numéro d\'immatriculation du véhicule', NULL, NULL),
(702, 'tripinformantion', 'Trip Informantion', 'Informations sur le voyage', NULL, NULL),
(703, 'updatesetting', 'Update Setting', 'Mise à jour des paramètres', NULL, NULL),
(704, 'title', 'Title', 'Titre', NULL, NULL),
(705, 'footertext', 'Footer Text', 'Texte de pied de page', NULL, NULL),
(706, 'update', 'Update', 'Mise à jour', NULL, NULL),
(707, 'fullname', 'Full Name', 'Nom complet', NULL, NULL),
(708, 'username', 'Username', 'Nom d\'utilisateur', NULL, NULL),
(709, 'password', 'Password', 'Mot de passe', NULL, NULL),
(710, 'repeatpassword', 'Repeat Password', 'Répéter le mot de passe', NULL, NULL),
(711, 'usertype', 'User Type', 'Type d\'utilisateur', NULL, NULL),
(712, 'selecttype', 'Select Type', 'Sélectionnez le type', NULL, NULL),
(713, 'superadmin', 'Super Admin', 'Super Admin', NULL, NULL),
(714, 'lastlogin', 'Last login', 'Dernière connexion', NULL, NULL),
(715, 'referencecellnumber', 'Reference Cell Number', 'Numéro de cellule de référence', NULL, NULL),
(716, 'expenseupdate', 'Expense Update', 'Mise à jour des dépenses', NULL, NULL),
(717, 'deactivate', 'Deactivate', 'Désactiver', NULL, NULL),
(718, 'vatincluded', 'Vat Included', 'TVA comprise', NULL, NULL),
(719, 'vatexcluded', 'Vat Excluded', 'TVA exclue', NULL, NULL),
(720, 'kilometers', 'Kilo Meters', 'Kilomètres', NULL, NULL),
(721, 'miles', 'Miles', 'Miles', NULL, NULL),
(722, 'totaladvance', 'Total Advance', 'Avance totale', NULL, NULL),
(723, 'totalbalance', 'Total Balance', 'Solde total', NULL, NULL),
(724, 'totalexpens', 'Total Expens', 'Dépense totale', NULL, NULL),
(725, 'updatelocaltripentry', 'Update Local Trip Entry', 'Mettre à jour l\'entrée du voyage local', NULL, NULL),
(726, 'tripinformation', 'Trip Information', 'Informations sur le voyage', NULL, NULL),
(727, 'oldpassword', 'Old Password', 'Ancien mot de passe', NULL, NULL),
(728, 'newpassword', 'New Password', 'Nouveau mot de pase', NULL, NULL),
(729, 'logo', 'Logo', 'Logo', NULL, NULL),
(730, 'favicon', 'Favicon', 'favicon', NULL, NULL),
(731, 'addlogo', 'Add Logo', 'Ajouter le logo', NULL, NULL),
(732, 'updatelogo', 'Update Logo', 'Mettre à jour le logo', NULL, NULL),
(733, 'position', 'Position', 'Position', NULL, NULL),
(735, 'managelogo', 'Manage Logo', 'Gérer le logo', NULL, NULL),
(736, 'stateinformation', 'State Information', 'Informations sur l\'État', NULL, NULL),
(737, 'distense', 'Distense', 'Distance', NULL, NULL),
(738, 'logomanage', 'Logo Manage', 'Gérer le logo', NULL, NULL),
(739, 'statename', 'State Name', 'Nom de l\'état', NULL, NULL),
(740, 'updatesuccessfully', 'Update Successfully.', 'Mise à jour avec succès.', NULL, NULL),
(741, 'savesuccessfully', 'Save Successfully', 'Sauvegarder avec succès', NULL, NULL),
(742, 'only_png_jpg_jpeg_file_selected', 'Only PNG,JPG,JPEG File Selected.', 'Seuls les fichiers PNG, JPG, JPEG sont sélectionnés', NULL, NULL),
(743, 'deletesuccessfully', 'Delete Successfully', 'Supprimer avec succès', NULL, NULL),
(744, 'fillupallrequiredfields', 'Fillup all required fields', 'Remplissez tous les champs obligatoires', NULL, NULL),
(746, 'registrationdate', 'Registration Date', 'Date d\'enregistrement', NULL, NULL),
(747, 'fitnessdate', 'Fitness Date', 'Date d\'aptitude', NULL, NULL),
(748, 'insurancedate', 'Insurance Date', 'Date d\'assurance', NULL, NULL),
(749, 'taxtokendate', 'Tax Token Date', 'Date du jeton d\'impôt', NULL, NULL),
(750, 'rootpermitdate', 'Root Permit Date', 'Date du permis racine', NULL, NULL),
(751, 'alltriplist', 'All Trip List', 'Liste de tous les voyages', NULL, NULL),
(752, 'dailytriplist', 'Daily Trip List', 'Liste des voyages quotidiens', NULL, NULL),
(753, 'payordernumber', 'Payorder Number', 'Numéro d\'ordre de paiement', NULL, NULL),
(754, 'payordernumber', 'Pay Order Number', 'Numéro d\'ordre de paiement', NULL, NULL),
(755, 'payordernumber', 'Payorder Number', 'Numéro d\'ordre de paiement', NULL, NULL),
(756, 'moneyreceipt', 'Money Receipt', 'Reçu d\'argent', NULL, NULL),
(757, 'serialno', 'Serial No', 'Numéro de série', NULL, NULL),
(759, 'voucher', 'Voucher', 'Bon d\'achat', NULL, NULL),
(760, 'phone', 'Phone', 'Téléphone', NULL, NULL),
(761, 'fax', 'Fax', 'Fax', NULL, NULL),
(762, 'paymentfrom', 'Payment From', 'Paiement de', NULL, NULL),
(763, 'signatureofpaymentgiver', 'Signature of Payment Giver', 'Signature du donneur d\'ordre', NULL, NULL),
(764, 'signatureofreceipient', 'Signature of Receipient', 'Signature du bénéficiaire', NULL, NULL),
(765, 'credit', 'Credit', 'Crédit', NULL, NULL),
(766, 'debit', 'Debit', 'Débit', NULL, NULL),
(767, 'dashboard', 'Dashboard', 'Tableau de bord', NULL, NULL),
(768, 'drivernid', 'NID scan', 'Scan de la CNI', NULL, NULL),
(769, 'driverlicense', 'Driver license picture', 'Scan du permis de conduire', NULL, NULL),
(770, 'cniexpdate', 'NID expiry date', 'Date d\'expiration de la CNI', NULL, NULL),
(771, 'postcode', 'Postal code', 'Code Postal', NULL, NULL),
(772, 'tradereg', 'Trade registration', 'Registre de commerce', NULL, NULL),
(773, 'tradeimmac', 'Trade registry identification', 'Immatriculation au registre de commerce', NULL, NULL),
(774, 'tax', 'Tax folder', 'Dossier fiscal', NULL, NULL),
(775, 'fournisseurs', 'Providers', 'Fournisseurs', NULL, NULL),
(776, 'listefournisseurs', 'Providers list', 'Liste des Fournisseurs', NULL, NULL),
(777, 'providerupdate', 'Update Provider', 'Mise à jour du fournisseur', NULL, NULL),
(778, 'providercreate', 'Add Provider', 'Ajouter un Fournisseur', NULL, NULL),
(779, 'providerupdate', 'Update Provider', 'Mise à jour du fournisseur', NULL, NULL),
(780, 'providerupdate', 'Update Provider', 'Mise à jour du fournisseur', NULL, NULL),
(781, 'stock', 'Stock management', 'Gestion des stocks', NULL, NULL),
(782, 'piecelist', 'List of vehicles parts', 'Liste des pièces de véhicules', NULL, NULL),
(783, 'vehiclemaintenance', 'Vehicles maintenance', 'Maintenance des véhicules', NULL, NULL),
(784, 'facturation', 'Billing', 'Facturation', NULL, NULL),
(785, 'billinglist', 'Bills list', 'Liste des factures', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lg_setting`
--

CREATE TABLE `lg_setting` (
  `id` int(11) NOT NULL,
  `language` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lg_setting`
--

INSERT INTO `lg_setting` (`id`, `language`) VALUES
(1, 'francais'),
(2, 'francais');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `logo_id` int(11) NOT NULL,
  `d_picture` text,
  `f_picture` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`logo_id`, `d_picture`, `f_picture`) VALUES
(10, 'bm_logo_big.jpeg', 'bm_logo.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE `provider` (
  `company_id` int(11) UNSIGNED NOT NULL,
  `company_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `company_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `company_postcode` int(10) DEFAULT NULL,
  `company_trade_reg` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `company_registration` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `company_cell` varchar(100) DEFAULT NULL,
  `company_email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company_web` varchar(100) CHARACTER SET latin1 NOT NULL,
  `company_tax` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `posting_id` int(10) NOT NULL,
  `active` int(10) NOT NULL COMMENT '0 = inactive, 1 = active, 2 = delete'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`company_id`, `company_name`, `company_address`, `company_postcode`, `company_trade_reg`, `company_registration`, `company_cell`, `company_email`, `company_web`, `company_tax`, `posting_id`, `active`) VALUES
(1, 'bdtask', '237 MacDonald Street\r\nWhataupoko\r\nGisborne 4010', NULL, NULL, NULL, '(027) 3746-722', 'JaimeBThomas@teleworm.us', 'PopBooth.co.nz', NULL, 1, 1),
(2, 'Asiatic Solutions', '38 Link Road\r\nCAPE BARREN ISLAND TAS 7257', NULL, NULL, NULL, '01710410490', 'JosephJCortez@dayrep.com', 'GamingCloset.com.au', NULL, 1, 1),
(3, 'Aaronson Furniture', '784 Tree Top Lane\r\nAllentown, PA 18109', NULL, NULL, NULL, '610-288-5853', 'NicholasMRoessler@teleworm.us', 'designerfences.com', NULL, 1, 1),
(4, 'MobileCostumes', 'Vennemansweg 152\r\n7251 HX  Vorden', NULL, NULL, NULL, '06-29952405', 'TinaKProvencher@armyspy.com', 'MobileCostumes.nl', NULL, 1, 1),
(5, 'Hills Supermarkets', '33, rue Saint Germain\r\n92230 GENNEVILLIERS', NULL, NULL, NULL, '01.15.36.32.26', 'CharlesMacvitie@rhyta.com', 'RegionalStore.fr', NULL, 1, 1),
(6, 'abcd', 'abcd', NULL, NULL, NULL, '78678678', 'abcd@gmail.com', 'abcd.com', NULL, 1, 2),
(7, 'abcd', 'dfghdh', NULL, NULL, NULL, '78678678', 'abcd@gmail.com', '', NULL, 1, 1),
(8, 'Megna Group', 'Dhaka Tatola', NULL, NULL, NULL, '65-75766', 'megna@gmail.com', 'megna.com', NULL, 1, 1),
(9, 'Amberit Group', 'Gulsha Dhaka', NULL, NULL, NULL, '78-6767', 'ambergroup@gmail.com', 'www.amber.com', NULL, 1, 1),
(10, 'Zumuna task', 'Dhaka', NULL, NULL, NULL, '898-7897897', 'task@gmail.com', 'www.task.com', NULL, 1, 2),
(11, 'Zumuna task', 'Dhaka', NULL, NULL, NULL, '898-7897897', 'task@gmail.com', 'www.task.com', NULL, 1, 1),
(12, 'name', '', NULL, NULL, NULL, '', '', '', NULL, 1, 2),
(13, 'test', 'test', NULL, NULL, NULL, '695425696', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2),
(14, 'tes 237', 'Ancienne Mairie - Etoug ébé', 5278, 'hesk.PNG', '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', 'hesk.PNG', 1, 2),
(15, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(16, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(17, '', '', NULL, NULL, NULL, NULL, '', '', NULL, 1, 2),
(18, 'name', 'Ancienne Mairie - Etoug ébé', 5278, NULL, '5435435435', '6394523646', 'lerrickjoubou@gmail.com', 'www.test.io', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `footer_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `title`, `address`, `footer_text`) VALUES
(1, 'Vehicle Managements', 'dhaka', 'footer text');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `state_name` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`state_id`, `state_name`, `posting_id`, `active`) VALUES
(5, 'Newyork', '1', 1),
(6, 'South Africa', '1', 1),
(7, 'Paris', '1', 1),
(8, 'Marseille', '1', 1),
(9, 'Strasbourg', '1', 1),
(10, 'Rangpur', '1', 1),
(11, 'England', '1', 1),
(12, 'Kolkata', '1', 1),
(13, 'Barisal', '1', 1),
(14, 'Dinajpur', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE `trip` (
  `id` int(11) NOT NULL,
  `trip_id` varchar(250) CHARACTER SET utf8 DEFAULT NULL,
  `trip_type` int(11) DEFAULT NULL,
  `import_export` int(3) NOT NULL,
  `driver_id` int(11) DEFAULT NULL,
  `v_id` int(11) DEFAULT NULL,
  `hire_v_id` varchar(255) DEFAULT NULL,
  `v_type_id` int(11) DEFAULT NULL,
  `start_dist_id` int(11) DEFAULT NULL,
  `start_station_id` int(11) DEFAULT NULL,
  `end_dist_id` int(11) DEFAULT NULL,
  `end_station_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `others_company` varchar(255) DEFAULT NULL,
  `date` date NOT NULL,
  `rent` float DEFAULT NULL,
  `fare_rent` float DEFAULT NULL,
  `advance` float DEFAULT NULL,
  `trip_link_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `posting_id` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`id`, `trip_id`, `trip_type`, `import_export`, `driver_id`, `v_id`, `hire_v_id`, `v_type_id`, `start_dist_id`, `start_station_id`, `end_dist_id`, `end_station_id`, `company_id`, `others_company`, `date`, `rent`, `fare_rent`, `advance`, `trip_link_id`, `posting_id`, `active`) VALUES
(430, 'BPCG1YRVKN', 1, 0, 8, 2, NULL, NULL, 5, 5, 7, 6, 1, NULL, '2020-06-26', 100000, 100000, 20000, 'M6TIB0NGUL', '1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) CHARACTER SET latin1 NOT NULL,
  `username` varchar(255) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `type` int(10) NOT NULL,
  `last_log_date` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `type`, `last_log_date`, `active`) VALUES
(1, 'Mr Admin', 'admin', 'e10adc3949ba59abbe56e057f20f883e', 9, '2020-06-27 12:24:16', 1),
(2, 'hasan', 'alhassan', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-03-29 10:40:33', 2),
(4, 'hasan', 'hasan', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-03-29 11:00:13', 1),
(5, 'ahmaed', 'bdtask', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-03-31 12:46:48', 1),
(6, 'rotinom', 'rotinom', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-04-02 05:03:44', 1),
(7, 'hasan', 'sumonhasan', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-04-03 12:17:34', 2),
(8, 'Roton', 'roton', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-04-08 01:03:46', 1),
(9, 'Jhan doye', 'johan', 'e10adc3949ba59abbe56e057f20f883e', 1, '2018-04-09 12:42:38', 1),
(10, 'Joubou Lerrick', 'brio', 'fadbe501c9a4f3f7fc3e594e1d6dfacd', 1, '2020-06-15 12:20:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_fitness`
--

CREATE TABLE `vehicle_fitness` (
  `v_fitness_id` int(10) UNSIGNED NOT NULL,
  `v_id` int(11) NOT NULL,
  `reg_issue` date NOT NULL,
  `reg_expire` date NOT NULL,
  `fitness_issue` date NOT NULL,
  `fitness_expire` date NOT NULL,
  `insurance_issue` date NOT NULL,
  `insurance_expire` date NOT NULL,
  `tax_issue` date NOT NULL,
  `tax_expire` date NOT NULL,
  `root_permit_issue` date NOT NULL,
  `root_permit_expire` date NOT NULL,
  `fuel_issue` date DEFAULT NULL,
  `fuel_expire` date DEFAULT NULL,
  `posting_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_fitness`
--

INSERT INTO `vehicle_fitness` (`v_fitness_id`, `v_id`, `reg_issue`, `reg_expire`, `fitness_issue`, `fitness_expire`, `insurance_issue`, `insurance_expire`, `tax_issue`, `tax_expire`, `root_permit_issue`, `root_permit_expire`, `fuel_issue`, `fuel_expire`, `posting_id`, `active`) VALUES
(1, 5, '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', 1, 1),
(2, 10, '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', '1970-01-01', 1, 1),
(3, 3, '2018-04-04', '2018-04-05', '2018-04-05', '2018-04-04', '2018-04-18', '2018-04-09', '2018-04-05', '2018-04-18', '2018-04-09', '2018-04-17', '1970-01-01', '1970-01-01', 1, 1),
(4, 6, '2018-04-05', '2018-04-04', '2018-04-05', '2018-04-06', '2018-04-12', '2018-04-04', '2018-04-06', '2018-04-04', '2018-04-27', '2018-04-23', '1970-01-01', '1970-01-01', 1, 1),
(5, 8, '2018-04-06', '2018-04-10', '2018-04-13', '2018-04-11', '2018-04-12', '2018-04-05', '2018-04-11', '2018-04-05', '2018-04-10', '2018-04-16', '1970-01-01', '1970-01-01', 1, 1),
(6, 10, '2018-04-04', '2018-04-16', '2018-04-20', '2018-04-11', '2018-04-28', '2018-04-12', '2018-04-12', '2018-04-13', '2018-04-16', '2018-04-27', '1970-01-01', '1970-01-01', 1, 1),
(7, 12, '2018-04-05', '2018-04-18', '2018-04-07', '2018-04-04', '2018-04-05', '1970-01-01', '2018-04-12', '2018-04-26', '2018-04-11', '2018-04-19', '1970-01-01', '1970-01-01', 1, 1),
(8, 5, '2018-04-20', '2018-04-10', '2018-04-04', '2018-04-18', '2018-04-19', '2018-04-17', '2018-04-18', '2018-04-04', '2018-04-05', '2018-04-09', '1970-01-01', '1970-01-01', 1, 1),
(9, 4, '2018-04-05', '2018-04-18', '2018-04-18', '2018-04-26', '2018-04-20', '2018-04-19', '2018-04-12', '2018-04-17', '2018-04-12', '2018-04-11', '1970-01-01', '1970-01-01', 1, 1),
(10, 5, '2018-04-12', '2018-04-18', '2018-04-19', '2018-04-05', '2018-04-12', '2018-04-11', '2018-04-18', '2018-04-19', '2018-04-19', '2018-04-18', '1970-01-01', '1970-01-01', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_fuel_rate`
--

CREATE TABLE `vehicle_fuel_rate` (
  `v_fuel_id` int(10) UNSIGNED NOT NULL,
  `v_id` int(100) NOT NULL COMMENT 'vehicle id',
  `v_fuel_per_kilo_litter` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_fuel_rate` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_fuel_last_update_dat` datetime NOT NULL,
  `posting_id` int(10) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_fuel_rate`
--

INSERT INTO `vehicle_fuel_rate` (`v_fuel_id`, `v_id`, `v_fuel_per_kilo_litter`, `v_fuel_rate`, `v_fuel_last_update_dat`, `posting_id`, `active`) VALUES
(1, 5, '130', '3000', '2018-04-03 04:18:51', 1, 1),
(2, 5, '100', '6000', '2018-04-03 04:16:34', 1, 1),
(3, 3, '500', '5000', '2018-04-03 04:16:51', 1, 1),
(4, 7, '500', '6000', '2018-04-03 04:19:11', 1, 1),
(5, 5, '140', '7000', '2018-04-03 04:19:27', 1, 1),
(6, 2, '333', '36000', '2018-04-04 07:01:24', 1, 1),
(7, 3, '90', '9000', '2018-04-07 10:55:13', 1, 1),
(8, 5, '300', '2000', '2018-04-07 10:55:35', 1, 1),
(9, 7, '401', '12000', '2018-04-07 10:55:52', 1, 1),
(10, 10, '40', '12000', '2018-04-07 10:56:09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_info`
--

CREATE TABLE `vehicle_info` (
  `v_id` int(10) UNSIGNED NOT NULL,
  `v_model_no` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_registration_no` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_chassis_no` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_engine_no` varchar(255) CHARACTER SET latin1 NOT NULL,
  `v_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` int(10) NOT NULL,
  `active` int(2) NOT NULL,
  `v_owner` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_info`
--

INSERT INTO `vehicle_info` (`v_id`, `v_model_no`, `v_registration_no`, `v_chassis_no`, `v_engine_no`, `v_type`, `posting_id`, `active`, `v_owner`) VALUES
(1, 'V-78', '56455645', '42345235', 'GH-56756756', '9', 1, 2, 1),
(2, '435452345', '4235234530', '25235443', '2354234544', '10', 1, 1, 0),
(3, '2352345', '23452342345', '25342345', '2542354', '11', 1, 1, 1),
(4, '567565', '6756756556', '56756755', '5675675', '11', 1, 1, 0),
(5, '5675687', '675675655', '56756755', '56756777', '13', 1, 1, 1),
(6, '56756566', '6756756556', '567567556', '567567655', '9', 1, 1, 1),
(7, '5675655', '6756756558', '56756755', '56756755', '13', 1, 1, 0),
(8, '82590136', '8259013607', '825901360', '82590136', '13', 1, 1, 0),
(9, '5675687', '675675655', '56756755', '5675677', '13', 1, 1, 1),
(10, '56756879', '82590136', '5675679', '56756759', '9', 1, 1, 1),
(11, '56756879', '675675659', '567567559', '56756759', '10', 1, 1, 1),
(12, '90909090', '90909090', '909090', '909090', '13', 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vehicle_type`
--

CREATE TABLE `vehicle_type` (
  `v_type_id` int(10) UNSIGNED NOT NULL,
  `v_type` varchar(255) CHARACTER SET latin1 NOT NULL,
  `posting_id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `active` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vehicle_type`
--

INSERT INTO `vehicle_type` (`v_type_id`, `v_type`, `posting_id`, `active`) VALUES
(7, 'vehicle type4', '1', 2),
(8, 'Coupe', '1', 1),
(9, 'Crossove', '1', 1),
(10, 'microbus', '1', 1),
(11, 'Truck', '1', 1),
(12, 'test', '1', 2),
(13, 'autocycle', '1', 1),
(14, 'camion', '1', 1),
(15, 'Crossove Van', '1', 1),
(16, 'test', '1', 2),
(17, 'test 007', '1', 2),
(18, 'Autocar', '1', 1),
(19, 'Autocar Max', '1', 1),
(20, 'Autocycle Motor', '1', 1),
(21, 'Truck Auto', '1', 1),
(22, 'Test 03', '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `web_setting`
--

CREATE TABLE `web_setting` (
  `id` int(11) NOT NULL,
  `values` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `web_setting`
--

INSERT INTO `web_setting` (`id`, `values`) VALUES
(1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acc_account`
--
ALTER TABLE `acc_account`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `acc_bank`
--
ALTER TABLE `acc_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `acc_company`
--
ALTER TABLE `acc_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `acc_inflow`
--
ALTER TABLE `acc_inflow`
  ADD PRIMARY KEY (`inflow_id`);

--
-- Indexes for table `acc_invoice`
--
ALTER TABLE `acc_invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `acc_outflow`
--
ALTER TABLE `acc_outflow`
  ADD PRIMARY KEY (`outflow_id`);

--
-- Indexes for table `add_company`
--
ALTER TABLE `add_company`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `city_city_distance`
--
ALTER TABLE `city_city_distance`
  ADD PRIMARY KEY (`distance_id`);

--
-- Indexes for table `company_rent`
--
ALTER TABLE `company_rent`
  ADD PRIMARY KEY (`company_rent_id`);

--
-- Indexes for table `driver_info`
--
ALTER TABLE `driver_info`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indexes for table `expense_data`
--
ALTER TABLE `expense_data`
  ADD PRIMARY KEY (`transection_id`);

--
-- Indexes for table `expense_list`
--
ALTER TABLE `expense_list`
  ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lg_setting`
--
ALTER TABLE `lg_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`logo_id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`company_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`state_id`);

--
-- Indexes for table `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vehicle_fitness`
--
ALTER TABLE `vehicle_fitness`
  ADD PRIMARY KEY (`v_fitness_id`);

--
-- Indexes for table `vehicle_fuel_rate`
--
ALTER TABLE `vehicle_fuel_rate`
  ADD PRIMARY KEY (`v_fuel_id`);

--
-- Indexes for table `vehicle_info`
--
ALTER TABLE `vehicle_info`
  ADD PRIMARY KEY (`v_id`);

--
-- Indexes for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  ADD PRIMARY KEY (`v_type_id`);

--
-- Indexes for table `web_setting`
--
ALTER TABLE `web_setting`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acc_account`
--
ALTER TABLE `acc_account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `acc_bank`
--
ALTER TABLE `acc_bank`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `acc_company`
--
ALTER TABLE `acc_company`
  MODIFY `company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `acc_inflow`
--
ALTER TABLE `acc_inflow`
  MODIFY `inflow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `acc_invoice`
--
ALTER TABLE `acc_invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `acc_outflow`
--
ALTER TABLE `acc_outflow`
  MODIFY `outflow_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `add_company`
--
ALTER TABLE `add_company`
  MODIFY `company_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `city_city_distance`
--
ALTER TABLE `city_city_distance`
  MODIFY `distance_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `company_rent`
--
ALTER TABLE `company_rent`
  MODIFY `company_rent_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `driver_info`
--
ALTER TABLE `driver_info`
  MODIFY `driver_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `expense_data`
--
ALTER TABLE `expense_data`
  MODIFY `transection_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2450;

--
-- AUTO_INCREMENT for table `expense_list`
--
ALTER TABLE `expense_list`
  MODIFY `expense_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=786;

--
-- AUTO_INCREMENT for table `lg_setting`
--
ALTER TABLE `lg_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `company_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `trip`
--
ALTER TABLE `trip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=431;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vehicle_fitness`
--
ALTER TABLE `vehicle_fitness`
  MODIFY `v_fitness_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vehicle_fuel_rate`
--
ALTER TABLE `vehicle_fuel_rate`
  MODIFY `v_fuel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `vehicle_info`
--
ALTER TABLE `vehicle_info`
  MODIFY `v_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `vehicle_type`
--
ALTER TABLE `vehicle_type`
  MODIFY `v_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `web_setting`
--
ALTER TABLE `web_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
