<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-bd lobidrag">
            <div class="panel-body">
            <div class="table-header">
                <i class="fa fa-list"></i>
                <?php echo display('piecelist'); ?> 
                <div class="pull-right btn btn-info">
                    <i class="fa fa-plus "></i>
                   <a style="color:white" href="<?php echo base_url(); ?>piece/create"><?php echo display('pieceadd') ?></a>
                </div>
            </div>
            <hr>
        </div>
        <?php
          if ($this->session->flashdata('success')) {
                echo "<div class=\"alert alert-success\" role=\"alert\">" . $this->session->flashdata('success') . "</div>";
           }
          ?>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th style="align:center;">
                                    <?php echo display('slno'); ?>
                                </th>

                                <th>
                                    <?php echo display('name'); ?>
                                </th>
                                <th>
                                    <?php echo display('brand'); ?>
                                </th> 

                                <th>
                                    <?php echo display('type'); ?>
                                </th> 

                                <th>
                                    <?php echo display('quantity'); ?>
                                </th> 

                                <th>
                                    <?php echo display('dateappro'); ?>
                                </th> 

                                <?php if ($this->session->userdata('user_type') == 9) { ?>
                                    <th class="no-print"><?php echo display('action'); ?></th>
                                <?php } //ends of if condition?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($pieces)) {
                                $count = 0;
                                foreach ($pieces as $piece) {
                                    ?>
                                    <tr>
                                        <td class="center"><?php echo $count + 1; ?></td>
                                        <td><?php echo $piece->name; ?></td>
                                        <td><?php echo $piece->brand; ?></td>
                                        <td><?php echo $piece->type; ?></td>
                                        <td><?php echo $piece->qty; ?></td>
                                        <td><?php echo date("d-m-Y", strtotime($piece->date_appro)); ?></td>

                <?php if ($this->session->userdata('user_type') == 9) { ?>
                                            <td class="no-print center">
                                            <a href="#<?php echo $piece->piece_id; ?>" class="danger led_btn" title="<?php echo display('pieceentry')?>" data-toggle="modal" data-target="#myModal">
                                                <i class="ace-icon fa fa-plus-circle bigger-130"></i>
                                            </a>&nbsp;&nbsp;
                                            <a href="#<?php echo $piece->piece_id; ?>" class="danger led_btn" title="<?php echo display('pieceout')?>" data-toggle="modal" data-target="#myModal2">
                                                <i class="ace-icon fa fa-minus-circle bigger-130"></i>
                                            </a>&nbsp;&nbsp;
                                                <a class="green" data-toggle="tooltip" title="<?php echo display('edit'); ?>" href="<?php echo base_url() . "piece/edit_piece/" . $piece->piece_id; ?>">
                                                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                                                </a>&nbsp;&nbsp;
                                                <a class="red delete" data-toggle="tooltip" title="<?php echo display('delete'); ?>"href="<?php echo base_url() . "piece/delete_piece/" . $piece->piece_id; ?>">
                                                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                                </a>
                                            </td> 
                                    <?php } //ends of user type check ?>

                                    </tr>
                                    <?php
                                    $count++;
                                }//foreach
                            } else {
                                echo '<tr>';
                                echo '<td colspan="7">' . display("nodatafound") . '</td>';
                                echo '</tr>';
                            }
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo display('pieceentry');?></h4></div>
            <div class="modal-body"> 
            <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="qty"><?php echo display('quantity'); ?> &nbsp;&nbsp; <span class="fa fa-asterisk red" style="color: red;"></span></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" name="qty" id="qty" class="form-control" placeholder="<?php echo display('quantity'); ?>"   value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" href="<?php echo base_url() . "piece/add_piece/" . $piece->piece_id; ?>">
                    <span class="fa fa-plus-circle" ></span>&nbsp;&nbsp;<?php echo display('add');?>
                </button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?php echo display('close');?></button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo display('pieceout');?></h4></div>
            <div class="modal-body"> 
            <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="qty"><?php echo display('quantity'); ?> &nbsp;&nbsp; <span class="fa fa-asterisk red" style="color: red;"></span></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="number" name="qty" id="qty" class="form-control" placeholder="<?php echo display('quantity'); ?>"   value="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-primary" href="<?php echo base_url() . "piece/remove_piece/" . $piece->piece_id; ?>">
                    <span class="fa fa-minus-circle" ></span>&nbsp;&nbsp;<?php echo display('remove');?>
                </button>
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal"><?php echo display('close');?></button>
            </div>
        </div>
    </div>
</div>