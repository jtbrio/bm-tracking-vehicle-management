<div class="col-sm-12">
    <div class="panel panel-bd lobidrag">
        <div class="panel-heading">
            <div class="panel-title">
                <h4>
                    <?php
                    if (!empty($pieces->piece_id)) {
                        echo display('pieceupdate');
                    } else {
                        echo display('pieceadd');
                    }
                    ?>
                </h4>
            </div>
        </div>
        <form name="test" class="form-horizontal" id="test" action="<?php echo base_url() . 'piece/save'; ?>" method="post">
            <div class="panel-body">           
                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name"><?php echo display('name'); ?>&nbsp;&nbsp;<span class="fa fa-asterisk red" style="color: red;"></span></label>

                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="name" id="name" class="form-control" placeholder="<?php echo display('name'); ?>"   value="<?php echo set_value('name', $pieces->name); ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="brand"><?php echo display('brand'); ?> &nbsp;&nbsp; <span class="fa fa-asterisk red" style="color: red;"></span></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                        <input type="text" name="brand" id="brand" class="form-control" placeholder="<?php echo display('brand'); ?>"   value="<?php echo set_value('brand', $pieces->brand); ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="type"><?php echo display('type'); ?> &nbsp;&nbsp; <span class="fa fa-asterisk red" style="color: red;"></span></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                            <input type="text" name="type" id="type" class="form-control" placeholder="<?php echo display('type'); ?>"   value="<?php echo set_value('type', $pieces->type); ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="qty"><?php echo display('quantity'); ?> &nbsp;&nbsp; <span class="fa fa-asterisk red" style="color: red;"></span></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="clearfix">
                        <input type="text" name="qty" id="qty"  placeholder="<?php echo display('quantity'); ?>" class="form-control" value="<?php echo set_value('qty', $pieces->qty); ?>" />
                        </div>
                    </div>
                </div>

                <div class="form-group">
                <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="date_appro"><?php echo display('dateappro'); ?></label>
                <div class="col-xs-12 col-sm-9">
                    <div class="clearfix">
                        <input type="text" name="date_appro" id="date_appro"  placeholder="<?php echo display('dateappro'); ?>"class="col-xs-12 col-sm-4 datepicker form-control"  value="<?php echo set_value('date_appro', $pieces->date_appro); ?>" />

                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label for="vehicle_type" class="col-sm-3 col-form-label"><?php echo display('inactive'); ?></label>
                <div class="col-sm-9">
                    <fieldset>
                        <div class="checkbox-circle">
                            <input name="active" type="radio" value="1" <?php echo set_radio('active', '1', TRUE); ?>>
                            <label for="checkbox7"><?php echo display('yes')?></label>

                            <input name="active" type="radio" value="0" <?php echo set_radio('active', '0'); ?>>
                            <label for="checkbox8"><?php echo display('no')?></label>
                        </div>

                    </fieldset>
                </div>
                <div class="help-block" id="title-exists"><?php echo form_error('active'); ?></div>
            </div>
                <br/>
                
                <input type="hidden" name="piece_id" id="piece_id" value="<?php echo set_value('piece_id', $pieces->piece_id); ?>"  />
                <div class="form-group row">
                    <div class="col-md-offset-1 col-md-9" style="margin-left: 40%;">
                    <a class="btn btn-danger w-md m-b-5" href="<?php echo base_url(); ?>piece"><?php echo display('cancel'); ?></a>
                        <button type="submit" class="btn btn-primary w-md m-b-5"><i class="fa fa-plus"></i> <?php echo display('save'); ?></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

