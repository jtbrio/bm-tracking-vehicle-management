<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Piece_model extends CI_Model{
	
	
	public function piece(){
		return $this->db->select('*')
			->from('piece')
			->where_not_in('active',2)
            ->order_by('piece_id','desc')
			->get()
			->result();
	}
	
	public function edit_piece($piece_id = ''){
		return $this->db->select('*')
			->from('piece')
			->where('piece_id',$piece_id)
			->get()
			->result();
	}
	
	function delete_piece($piece_id){
		return $this->db->set('active',2)
			->where('piece_id',$piece_id)
			->update('piece'); 
	}
	
	public function save($data){
		$data['posting_id'] = $this->session->userdata('user_id');
		if(!empty($data['piece_id'])){
			$this->db->where('piece_id',$data['piece_id']);
			$this->db->update('piece',$data);
		}else{
			$this->db->insert('piece',$data);
		}
	} 

	public function piece_name_list(){
		$query = $this->db->select('piece_id,name')
			->from('piece')
			->where('active',1)
			->order_by('name','asc')
			->get()
			->result();
		$piece[''] = lang("SELECT_PIECE_NAME"); 
		foreach($query as $value){
			$piece[$value->piece_id] = $value->name;
		}  
		return $piece;
	}
	

} 