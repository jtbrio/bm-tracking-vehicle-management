<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Piece extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if($user_id == NULL){redirect('admin');} 
	   $this->load->model('piece_model');
           
    }
	
	//==================this function for view comapany list (start) ========================//
	
	public function index($pdf = NULL){
	$data['m_pce'] = 'active';
	$data['pieces'] = $this->piece_model->piece(); 
        $data['content'] = $this->load->view('pages/piece_view',$data,TRUE);
		$this->load->view('wrapper_main',$data); 

		if($pdf == "pdf"):   
            ini_set('memory_limit', '256M'); 
            // $html = $this->output->get_output($html);
            $this->load->library('dompdf_gen'); 
            $this->dompdf->load_html($data['content']);
            $this->dompdf->render(); 
            $filename = strtoupper(date('d_M_Y').'_'.$this->uri->segment(1)."_".$this->uri->segment(3));
            $this->dompdf->stream($filename.".pdf");
        endif;  
	}
	//==================this function for view comapany list (start) ========================//


	//=============== This function for create piece (Start) =============================//
	public function create(){	 
	//$data['m_pce'] = 'active';
	$data['pieces'] = (object) array('piece_id'=>'',
	'name'=>'','brand'=>'',
	'type'=>'','qty'=>'','date_appro'=>'');
        $data['content'] = $this->load->view('pages/piece_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
		
	}
	
	//=============== This function for create piece (End) =============================//
	
	//=============== This function for Save piece (Start) =============================//
	
	
	public function save(){  
		//$data['m_pce'] = 'active';
		$piece_id = $this->input->post('piece_id');//this name for get piece id
		$name = trim($this->input->post('name'));//this name for get piece name
		$brand = trim($this->input->post('brand'));//this name for get piece address
		$type = trim($this->input->post('type'));
		$qty = $this->input->post('qty');
		$date_appro = date('Y-m-d',strtotime($this->input->post('date_appro')));
		$is_active = trim($this->input->post('active'));//this name for get piece web

		$data['pieces'] = (object) array('piece_id'=> $piece_id,
		'name'=> $name,
		'brand'=> $brand,
		'type'=> $type,
		'qty'=> $qty,
		'date_appro'=> $date_appro
		);
		
		if($this->form_validation->run() == FALSE){
		
		
	        $data['content'] = $this->load->view('pages/piece_form',$data,TRUE);
	        $this->load->view('wrapper_main',$data);
		} else{ 
			
			
		$savedata = array('piece_id'=>$piece_id,
							'name'=>$name,
							'brand'=>$brand,
							'type'=>$type,
							'qty'=>$qty,
							'date_appro'=>$date_appro,
							'active'=>$is_active
							); 	 
		// $data['piece_id'] = $this->input->post('piece_id');//this name for get piece id
		// $data['name'] = trim($this->input->post('name'));//this name for get piece name
		// $data['brand'] = trim($this->input->post('brand'));//this name for get piece address
		// $data['type'] = trim($this->input->post('type'));
		// $data['qty'] = $this->input->post('qty');
		// $data['date_appro'] = date('Y-m-d',strtotime($this->input->post('date_appro')));
		// $data['is_active'] = trim($this->input->post('active'));//this name for get piece web			

			$this->piece_model->save($savedata);
                       
			if(!empty($piece_id)){
				$this->session->set_flashdata('success', display('updatesuccessfully'));
			}
			else{
				$this->session->set_flashdata('success', display('savesuccessfully'));
			}
			redirect('piece');
		}		
	}
	//=============== This function for Save piece (End) =============================//
	
	//================this Function for edit Company(Start) ============================//
	
	
	public function edit_piece($piece_id=''){	
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
		#
		$data['m_pce'] = 'active';
		$pieceList = $this->piece_model->edit_piece($piece_id);	
		$data['pieces'] = $pieceList[0];
        $data['content'] = $this->load->view('pages/piece_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
	}
	//================this Function for edit Company(End) ============================//
	
	//================this Function for Delete Company(Start) ============================//
	
	public function delete_piece($piece_id=''){
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
                else{
                    $this->piece_model->delete_piece($piece_id);
                    $this->session->set_flashdata('success', display('deletesuccessfully'));
		    redirect('piece');
                }
		#
		
	}
	//================this Function for Delete Company(End) ============================//

	//================this Function for edit Company(Start) ============================//
	
	
	public function add_piece($piece_id=''){	
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
		#
		$data['m_pce'] = 'active';
		$pieceList = $this->piece_model->edit_piece($piece_id);	
		$data['pieces'] = $pieceList[0];
        $data['content'] = $this->load->view('pages/piece_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
	}
	//================this Function for edit Company(End) ============================//

	//================this Function for edit Company(Start) ============================//
	
	
	public function remove_piece($piece_id=''){	
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
		#
		$data['m_pce'] = 'active';
		$pieceList = $this->piece_model->edit_piece($piece_id);	
		$data['pieces'] = $pieceList[0];
        $data['content'] = $this->load->view('pages/piece_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
	}
	//================this Function for edit Company(End) ============================//
}
	
	