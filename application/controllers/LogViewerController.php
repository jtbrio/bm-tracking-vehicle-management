<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require('./CILogViewer.php');
class LogViewerController{
    private $logViewer;
    
    public function __construct() {    
        $this->logViewer = new CILogViewer();    
    }
    
    public function index() {    
        echo $this->logViewer->showLogs(); 
        return;   
    }
}