<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $user_id = $this->session->userdata('user_id');
        if($user_id == NULL){redirect('admin');} 
	   $this->load->model('company_model');
           
    }
	
	//==================this function for view comapany list (start) ========================//
	
	public function index($pdf = NULL){
	$data['m_cmpny'] = 'active';
	$data['companys'] = $this->company_model->company(); 
        $data['content'] = $this->load->view('pages/company_view',$data,TRUE);
		$this->load->view('wrapper_main',$data);
		
		if($pdf == "pdf"):   
            ini_set('memory_limit', '256M'); 
            // $html = $this->output->get_output($html);
            $this->load->library('dompdf_gen'); 
            $this->dompdf->load_html($data['content']);
            $this->dompdf->render(); 
            $filename = strtoupper(date('d_M_Y').'_'.$this->uri->segment(1)."_".$this->uri->segment(3));
            $this->dompdf->stream($filename.".pdf");
        endif;  
	}
	//==================this function for view comapany list (start) ========================//


	//=============== This function for create company (Start) =============================//
	public function create(){	 
	$data['m_cmpny'] = 'active';
	$data['companys'] = (object) array('company_id'=>'',
	'company_name'=>'','company_address'=>'',
	'company_postcode'=>'','company_trade_reg'=>'','company_registration'=>'',
	'company_cell'=>'','company_email'=>'','company_web'=>'','company_tax'=>'');
        $data['content'] = $this->load->view('pages/company_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
		
	}
	
	//=============== This function for create company (End) =============================//
	
	//=============== This function for Save company (Start) =============================//
	
	
	public function save(){  
		//$data['m_cmpny'] = 'active';
		$company_id = $this->input->post('company_id');//this name for get company id
		$company_name = trim($this->input->post('company_name'));//this name for get company name
		$company_address = trim($this->input->post('company_address'));//this name for get company address
		$company_postcode = trim($this->input->post('company_postcode'));
		$company_trade_reg = trim($this->input->post('company_trade_reg'));
		$company_registration = trim($this->input->post('company_registration'));
		$company_cell = trim($this->input->post('company_cell'));//this name for get company cell no
		$company_email = trim($this->input->post('company_email'));//this name for get company e-mail
		$company_web = trim($this->input->post('company_web'));//this name for get company web
		$company_tax = trim($this->input->post('company_tax'));//this name for get company web
		$is_active = trim($this->input->post('active'));//this name for get company web

		//============================ for form validation (start) ====================//
		$this->form_validation->set_rules('company_name','Company Name','trim|required'); 
		$this->form_validation->set_rules('company_address','Company Address','trim|required');
		$this->form_validation->set_rules('company_postcode','Company Postal Code','trim|required');  
		$this->form_validation->set_rules('company_registration','Company Registration','trim|required'); 
		$this->form_validation->set_rules('company_cell','Company Cell','trim|required'); 
		$this->form_validation->set_rules('company_email','Company E-Mail','trim|required');
		$this->form_validation->set_rules('company_trade_reg','Company Trade Registry','trim|required');
		$this->form_validation->set_rules('company_tax','Company Tax folder','trim|required');
		$this->form_validation->set_rules('active','Is Active','required'); 
		
		if($this->form_validation->run() == FALSE){
		$data['companys'] = (object) array('company_id'=> $company_id,
		'company_name'=> $company_name,
		'company_address'=> $company_address,
		'company_postcode'=> $company_postcode,
		'company_registration'=> $company_registration,
		'company_cell'=> $company_cell,
		'company_email'=> $company_email,
		'company_web'=> $company_web,
		'active'=>$is_active
	
		);
		
	        $data['content'] = $this->load->view('pages/company_form',$data,TRUE);
	        $this->load->view('wrapper_main',$data);
		} else{ 
			
			$image=$_FILES['company_trade_reg']['name'];
			if (!empty($image))
				{
					$config['allowed_types'] = 'jpg|jpeg|png'; 
					$config['upload_path'] = 'assets/company/';
					$this->load->library('upload', $config);

				if (!$this->upload->do_upload('company_trade_reg')){
					$this->form_validation->set_message('image_upload', $this->upload->display_errors());
					$this->session->set_flashdata('error', display('onlypngjpgjpegfileselected'));
					redirect('company/create');
				}   
					else{
						$upload_data	= $this->upload->data();
						$data['company_trade_reg']	= $upload_data['file_name'];
					}
				}	

				$image2=$_FILES['company_tax']['name'];
				if (!empty($image2))
					{
					$config['allowed_types'] = 'jpg|jpeg|png'; 
					$config['upload_path'] = 'assets/company/';
					$this->load->library('upload', $config);

					if (!$this->upload->do_upload('company_tax')){
						$this->form_validation->set_message('image_upload', $this->upload->display_errors());
						$this->session->set_flashdata('error', display('onlypngjpgjpegfileselected'));
						redirect('company/create');
					}   
					else{
							$upload_data	= $this->upload->data();
							$data['company_tax']	= $upload_data['file_name'];
					}
				}

			// $data = array('company_id'=>$company_id,
			// 					'company_name'=>$company_name,
			// 					'company_address'=>$company_address,
			// 					'company_postcode'=>$company_postcode,
			// 					'company_registration'=>$company_registration,
			// 					'company_cell'=>$company_cell,
			// 					'company_email'=>$company_email,
			// 					'company_web'=>$company_web,
			// 					'active'=>$is_active
			// 					); 	 
			
			$data['company_id'] = $this->input->post('company_id');//this name for get company id
			$data['company_name'] = trim($this->input->post('company_name'));//this name for get company name
			$data['company_address'] = trim($this->input->post('company_address'));//this name for get company address
			$data['company_postcode'] = trim($this->input->post('company_postcode'));
			$data['company_registration'] = trim($this->input->post('company_registration'));
			$data['company_cell'] = trim($this->input->post('company_cell'));//this name for get company cell no
			$data['company_email'] = trim($this->input->post('company_email'));//this name for get company e-mail
			$data['company_web'] = trim($this->input->post('company_web'));//this name for get company web
			$data['active'] = trim($this->input->post('active'));//this name for get company web

			

			$this->company_model->save($data);
                       
                        if(!empty($company_id)){
                            $this->session->set_flashdata('success', display('updatesuccessfully'));
                        }
                        else{
                            $this->session->set_flashdata('success', display('savesuccessfully'));
                        }
			redirect('company');
		}		
	}
	//=============== This function for Save company (End) =============================//
	
	//================this Function for edit Company(Start) ============================//
	
	
	public function edit_company($company_id=''){	
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
		#
		$data['m_cmpny'] = 'active';
		$companyList = $this->company_model->edit_company($company_id);	
		$data['companys'] = $companyList[0];
        $data['content'] = $this->load->view('pages/company_form',$data,TRUE);
        $this->load->view('wrapper_main',$data);
	}
	//================this Function for edit Company(End) ============================//
	
	//================this Function for Delete Company(Start) ============================//
	
	public function delete_company($company_id=''){
		if($this->session->userdata('isLogin') == FALSE 
			|| $this->session->userdata('user_type')!=9) {
			redirect('admin');
		}
                else{
                    $this->company_model->delete_company($company_id);
                    $this->session->set_flashdata('success', display('deletesuccessfully'));
		    redirect('company');
                }
		#
		
	}
	//================this Function for Delete Company(End) ============================//
 
}
	
	